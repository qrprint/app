Accessible ici : https://qrprint.forge.apps.education.fr/app/

# Générateur de QR Code avec personnalisation et génération de grille imprimable à découper

## Description

Cette application permet de générer des QR Codes avec un **titre personnalisé** et offre la possibilité de **générer une grille** imprimable contenant plusieurs QR Codes à découper. Le style des QR Codes est entièrement personnalisable, y compris les **couleurs**, la **forme des points**, et les **coins** du code. L'application permet d'exporter la grille en **PDF**.

L'application utilise les bibliothèques suivantes :
- **QR Code Styling** : pour personnaliser le style des QR Codes.
- **jsPDF** : pour générer un fichier PDF imprimable avec une grille de QR Codes.
- **html2canvas** : pour capturer l'aperçu du QR Code et l'exporter sous forme d'image.

## Fonctionnalités

- Génération de QR Codes avec un titre.
- Personnalisation du QR Code (couleurs, formes des points, et coins).
- Génération de grilles de QR Codes à imprimer et découper.
- Export des QR Codes en format **PNG**, **SVG** ou directement dans le **Presse-papier**.
- Interface simple et intuitive.

## Bibliothèques utilisées

- [QR Code Styling](https://github.com/kozakdenys/qr-code-styling) : Personnalisation avancée des QR Codes.
- [jsPDF](https://github.com/parallax/jsPDF) : Génération de fichiers PDF.
- [html2canvas](https://html2canvas.hertzen.com/) : Conversion d'éléments HTML en images.

## Utilisation

1. Entrez les données que vous souhaitez intégrer dans le QR Code.
2. Ajoutez un titre si vous souhaitez l'ajouter en dessous du QR Code.
3. Personnalisez les couleurs et les formes du QR Code à l'aide du panneau de personnalisation.
5. Générez une **grille de QR Codes** en entrant le nombre de lignes et de colonnes souhaitées.
6. Exportez votre QR Code.

## Export

Vous pouvez exporter le QR Code ou la grille générée dans plusieurs formats :

- **PNG** : Image PNG avec gestion de la transparence.
- **SVG** : Format vectoriel pour une qualité d'image optimale (récupère uniquement le QR Code sans le cadre de titre).
- **Presse-papier** : Copie de l'image PNG dans le presse-papier.
- **PDF** : Grille imprimable de QR Codes à découper.

## Licence

Ce projet est proposé sous la licence [MIT](LICENSE). Vous êtes libre de l'utiliser, le modifier et le distribuer sous les conditions de la licence.

---

**Contributeurs :** Si vous souhaitez contribuer à ce projet, n'hésitez pas à ouvrir un ticket pour toute amélioration ou correction.
