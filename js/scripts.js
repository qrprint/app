// ------------------------------------
// Initialisation du QR Code et paramètres par défaut
// ------------------------------------

const tabs = {};
let currentTabId = 'tab-1'; // Onglet actif initial
let multiple = false; // Indique s'il y a plusieurs onglets
let tabCount = 1; // Compteur pour nommer les onglets

// Initialisation de l'instance QRCodeStyling avec les options par défaut pour le premier onglet
tabs['tab-1'] = {
    qrCode: new QRCodeStyling({
        width: 400,
        height: 400,
        data: "QRPrint",
        image: "",
        dotsOptions: {
            color: "#6a1a4c",
            type: "extra-rounded"
        },
        cornersSquareOptions: {
            color: "#000000",
            type: "extra-rounded"
        },
        cornersDotOptions: {
            color: "#000000",
            type: "dot"
        },
        backgroundOptions: {
            color: "#ffffff",
        },
        imageOptions: {
            crossOrigin: "anonymous",
            margin: 0
        },
        qrOptions: {
            mode: "Byte",
            errorCorrectionLevel: "Q"
        }
    }),
    data: {
        data: "QRPrint",
        title: "Scannez-moi",
        subtitle: "QRPrint",
        customsubtitle: false,
        includeData: true,
        includeTitle: true,
    }
};

// Ajout du QR code à l'aperçu du premier onglet
tabs['tab-1'].qrCode.append(document.getElementById("qrPreview-tab-1"));

// Définition du titre par défaut pour le premier onglet
document.getElementById("form-title-text-tab-1").value = "Scannez-moi";



// ------------------------------------
// Références aux éléments du DOM
// ------------------------------------

// Références aux éléments du menu contextuel
const customContextMenu = document.getElementById("customContextMenu");
const contextSavePng = document.getElementById("context-save-png");
const contextSaveSvg = document.getElementById("context-save-svg");
const contextCopyClipboard = document.getElementById("context-copy-clipboard");


// Éléments liés à la fenêtre modale d'exportation
const exportFileNameModal = document.getElementById('exportFileNameModal');
const exportFileNameInput = document.getElementById('exportFileName');
const confirmExportBtn = document.getElementById('confirmExportBtn');
const closeExportModalBtn = exportFileNameModal.querySelector('.close');


// Références aux éléments de la modale d'importation CSV
const importCsvModal = document.getElementById('importCsvModal');
const csvFileInput = document.getElementById('csvFileInput');
const csvDataTextarea = document.getElementById('csvData');
const validateImportBtn = document.getElementById('validateImportBtn');


// Références aux éléments des options de la grille
const multipleOptions = document.getElementById('multipleOptions');
const useGlobalQuantityCheckbox = document.getElementById('useGlobalQuantity');
const globalQuantityContainer = document.getElementById('globalQuantityContainer');
const placementTypeContainer = document.getElementById('placementTypeContainer');
const placementTypeSelect = document.getElementById('placementType');
const lineBreakContainer = document.getElementById('lineBreakContainer');
const lineBreakCheckbox = document.getElementById('lineBreak');
const globalQuantityInput = document.getElementById('globalQuantity');

const rowsInput = document.getElementById('rows');
const columnsInput = document.getElementById('columns');
const dottedlines = document.getElementById('dottedlines');

// Référence au mode MultipleQRCode
const MultipleQRCodeMode = document.getElementById('multipleQRCodeOptionInput');

// Variables pour les options de la grille
let linebreak = lineBreakCheckbox.checked;
let globalPrint = useGlobalQuantityCheckbox.checked;
let globalNumber = globalQuantityInput.value;
let placementType = placementTypeSelect.value;


// ------------------------------------
// Objet UI pour la gestion de la superposition de chargement
// ------------------------------------

const loadingOverlay = document.getElementById('loadingOverlay');

const UI = {

    /**
     * Affiche la superposition de chargement pour indiquer que le traitement est en cours.
     */
    showLoadingOverlay: function() {
        loadingOverlay.style.display = 'flex';
    },

    /**
     * Masque la superposition de chargement une fois le traitement terminé.
     */
    hideLoadingOverlay: function() {
        loadingOverlay.style.display = 'none';
    }

}

// ------------------------------------
// Écouteurs pour les entrées de lignes et colonnes
// ------------------------------------

// Mise à jour de l'image lors du changement des valeurs
rowsInput.addEventListener('change', async function(e) {
  restrictInput(e,1,10,"rows_cols");
    UI.showLoadingOverlay();
    await new Promise(resolve => requestAnimationFrame(resolve));
    generateImage();
});
columnsInput.addEventListener('change', async function(e) {
  restrictInput(e,1,10,"rows_cols");
    UI.showLoadingOverlay();
    await new Promise(resolve => requestAnimationFrame(resolve));
    generateImage();
});

globalQuantityInput.addEventListener('change', async function(e) {
  restrictInput(e,1,99,"gbQuantity");
});



// Ajout des écouteurs d'événements pour la validation en temps réel
rowsInput.addEventListener('keydown', restrictInputToNumbers);
columnsInput.addEventListener('keydown', restrictInputToNumbers);
globalQuantityInput.addEventListener('keydown', restrictInputToNumbers);

rowsInput.addEventListener('paste', handlePaste);
columnsInput.addEventListener('paste', handlePaste);
globalQuantityInput.addEventListener('paste', handlePaste);


function restrictInput(event,min,max,type) {
    const input = event.target;
    const value = parseInt(input.value, 10);

    // Si la valeur est inférieure à 1, la fixer à 1
    if (value < min) {
        input.value = min;
        if (type=="rows_cols"){
        alert("Le nombre ne peut être inférieur à 1.");}
    }

    // Si la valeur est supérieure à 9, la fixer à 9
    if (value > max) {
        input.value = max;
        if (type=="rows_cols"){
        alert("Le nombre ne peut être supérieur à 10 car les QR code seraient alors trop petits pour l'impression et donc inutilisables.");}
    }

    // Si la valeur n'est pas un entier, la convertir en entier
    if (!Number.isInteger(value)) {
        input.value = Math.round(value);
    }

    if (input.value=="") {
        input.value = Math.floor((max-min)/2);
        if (type=="rows_cols"){
        alert("Ce n'est pas une valeur correcte. Nombre réglé à 5 par défaut.");}
    }
}





function restrictInputToNumbers(event) {
    const key = event.key;

    // Autoriser les touches de contrôle : backspace, delete, flèches, tab, etc.
    const controlKeys = [
        'Backspace', 'Delete', 'ArrowLeft', 'ArrowRight', 'Tab',
        'Home', 'End', 'Enter'
    ];

    if (controlKeys.includes(key)) {
        return; // Autoriser ces touches
    }

    // Autoriser seulement les chiffres de 1 à 9
    if (!/^[0-9]$/.test(key)) {
        event.preventDefault(); // Empêcher la saisie
    }
}

// Fonction pour empêcher le collage de caractères non numériques
function handlePaste(event) {
    const pasteData = event.clipboardData.getData('text');
    if (!/^[0-9]$/.test(pasteData)) {
        event.preventDefault(); // Empêcher le collage
    }
}



// ------------------------------------
// Fonctions pour Ajouter et Supprimer des Onglets
// ------------------------------------

// Fonction pour ajouter un nouvel onglet
function addTab(data = "", title = "Scannez-moi",subtitle = "", number = 1, force = false)  {
    const liElements = document.querySelectorAll('#qrTabs li');
    const count = liElements.length;
    if (count > 20 && !force) {
        alert("Vous avez atteint le nombre maximum d'onglets (20).");
    } else {

        tabCount++;
        const newTabId = `tab-${tabCount}`;
        const tabName = `${tabCount}`;

        // Ajouter un nouvel onglet à la navigation
        const tabsNav = document.getElementById('qrTabs');
        const newTab = document.createElement('li');
        newTab.classList.add('nav-item');
        newTab.setAttribute('role', 'presentation');
        newTab.innerHTML = `
      <button class="nav-link" id="${newTabId}-tab" data-bs-toggle="tab" data-bs-target="#${newTabId}" type="button" role="tab" aria-controls="${newTabId}" aria-selected="false">
        ${tabName}
        <span class="btn-close ms-2" aria-label="Close" onclick="removeTab('${newTabId}')"></span>
      </button>
    `;
        // Insert avant le bouton "+"
        tabsNav.insertBefore(newTab, tabsNav.lastElementChild);

        // Ajouter un nouveau contenu d'onglet
        const tabsContent = document.getElementById('qrTabsContent');
        const newTabPane = document.createElement('div');
        newTabPane.classList.add('tab-pane', 'fade');
        newTabPane.id = newTabId;
        newTabPane.setAttribute('role', 'tabpanel');
        newTabPane.setAttribute('aria-labelledby', `${newTabId}-tab`);
        newTabPane.innerHTML = `
      <form class="qr-form" id="form-${newTabId}">
        <!-- Accordéon -->
        <div class="accordion" id="qrAccordion-${newTabId}">
          <!-- Options Principales -->
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingMain-${newTabId}">
              <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseMain-${newTabId}" aria-expanded="true" aria-controls="collapseMain-${newTabId}"> Données </button>
            </h2>
            <div id="collapseMain-${newTabId}" class="accordion-collapse collapse show" aria-labelledby="headingMain-${newTabId}" data-bs-parent="#qrAccordion-${newTabId}">
              <div class="accordion-body">
                <input id="form-data-${newTabId}" type="text" value="${data}" placeholder="https://... ou message texte ..." class="form-control mb-3">
                <div class="form-check form-switch mb-3">
                  <input class="form-check-input" type="checkbox" id="form-title-include-data-${newTabId}" checked>
                  <label class="form-check-label" for="form-title-include-data-${newTabId}" style="margin-left: 20px;height: 18px;">Afficher sous le QRCode</label>
                </div>
                <hr>
                <div class="form-check form-switch mb-3">
                  <input class="form-check-input" type="checkbox" id="form-title-include-title-${newTabId}" checked>
                  <label class="form-check-label" for="form-title-include-title-${newTabId}" style="margin-left: 20px;height: 18px;">Cadre de titre</label>
                </div>
                <div id="title-input-${newTabId}" class="mb-3">
                  <input id="form-title-text-${newTabId}" type="text" value="${title}" class="form-control" maxlength="25">
                </div>
              </div>
            </div>
          </div>
          <!-- Options des Points -->
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingDots-${newTabId}">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseDots-${newTabId}" aria-expanded="false" aria-controls="collapseDots-${newTabId}"> Apparence </button>
            </h2>
            <div id="collapseDots-${newTabId}" class="accordion-collapse collapse" aria-labelledby="headingDots-${newTabId}" data-bs-parent="#qrAccordion-${newTabId}">
              <div class="accordion-body">
                <!-- Contenu identique au formulaire principal, avec des IDs uniques -->
                <h3>Couleurs externes</h3>
                <div class="mb-3" id="form-border-style-${newTabId}">
                  <img src="./img/ccadre.png" width="50px" height="57px" style="margin-right: 10px;">
                  <label style="font-size: 1.4rem;width: 100%;">Cadre ></label>
                  <div class="d-flex align-items-center">
                    <input id="form-title-frame-color-${newTabId}" type="color" value="#000000" class="form-control form-control-color me-3" title="Choisir la couleur du cadre">
                  </div>
                </div>
                 <div class="mb-3" id="form-title-style-${newTabId}">
                      <img src="./img/ctitle.png" width="50px" height="57px" style="margin-right: 10px;">
                      <label style="font-size: 1.4rem;width: 100%;">Titre ></label>
                      <div class="d-flex align-items-center">
                        <input id="form-title-text-color-${newTabId}" type="color" value="#ffffff" class="form-control form-control-color me-3" title="Choisir la couleur du texte">
                      </div>
                    </div>
                    <div class="mt-3">
                      <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="export-transparent-bg-${newTabId}">
                        <label class="form-check-label" for="export-transparent-bg" style="margin-left: 20px;height: 18px;">Fond transparent</label>
                      </div>
                    </div>
                    <div class="mb-3" id="form-background-color-div-${newTabId}">
                      <img src="./img/cbackground.png" width="50px" height="45px" style="margin-right: 10px;">
                      <label style="font-size: 1.4rem;width: 100%;">Fond ></label>
                      <div class="d-flex align-items-center">
                        <input id="form-background-color-${newTabId}" type="color" value="#ffffff" class="form-control form-control-color me-3" title="Choisir une couleur">
                      </div>
                    </div>
                    <hr>
                    <h3>Logo</h3>
                    <div class="buttons-container mb-3">
                      <input id="form-image-file-${newTabId}" type="file" accept="image/*" class="form-control me-2">
                      <button type="button" id="button-clear-image-${newTabId}" class="btn btn-danger">Effacer</button>
                      <div class="mb-3" id="form-image-size-group-${newTabId}">
                        <label for="form-image-size" class="form-label" style="padding-top: 10px;padding-right: 8px;">Taille du logo</label>
                        <input id="form-image-size-${newTabId}" type="number" min="1" max="3" step="1" value="2" class="form-control" style="width: 70px;">
                      </div>
                    </div>
                    <hr>
                    <h3>Format de la matrice</h3>
                    <div class="mb-3">
                      <img src="./img/ccode.png" width="50px" height="50px">
                      <select id="form-dots-type-${newTabId}" class="form-select">
                        <option value="square" selected>Carré</option>
                        <option value="dots">Points</option>
                        <option value="rounded">Arrondis</option>
                        <option value="extra-rounded">Très Arrondis</option>
                        <option value="classy">Élégant</option>
                        <option value="classy-rounded">Élégant arrondi</option>
                      </select>
                      <div class="d-flex align-items-center">
                        <input id="form-dots-color-${newTabId}" type="color" value="#000" class="form-control form-control-color me-3" title="Choisir une couleur">
                      </div>
                    </div>
                    <div class="mb-3">
                      <img src="./img/ccoin2.png" width="50px" height="50px">
                      <select id="form-corners-square-type-${newTabId}" class="form-select">
                        <option value="square" selected>Carré</option>
                        <option value="dot">Cercle</option>
                        <option value="extra-rounded">Arrondi</option>
                      </select>
                      <div class="d-flex align-items-center">
                        <input id="form-corners-square-color-${newTabId}" type="color" value="#000000" class="form-control form-control-color me-3" title="Choisir une couleur">
                      </div>
                    </div>
                    <div class="mb-3">
                      <img src="./img/ccoin.png" width="50px" height="50px">
                      <select id="form-corners-dot-type-${newTabId}" class="form-select">
                        <option value="square" selected>Carré</option>
                        <option value="dot">Point</option>
                      </select>
                      <div class="d-flex align-items-center">
                        <input id="form-corners-dot-color-${newTabId}" type="color" value="#000000" class="form-control form-control-color me-3" title="Choisir une couleur">
                      </div>
                    </div>
                    <hr>
                    <div class="mb-3">
                      <label for="form-qr-error-correction-level" class="form-label">Niveau de correction d'erreur</label>
                      <select id="form-qr-error-correction-level-${newTabId}" class="form-select">
                        <option value="L">Faible</option>
                        <option value="M">Moyen</option>
                        <option value="Q" selected>Élevé</option>
                        <option value="H">Très élevé</option>
                      </select>
                    </div>          

              </div>
            </div>
          </div>
          <div style="padding-top: 10px; display: none;" id="print-number-div-${newTabId}">Nombre à imprimer : 

          <div class="input-number-wrapper">
                      <input type="number" id="print-number-${newTabId}" value="${number}" min="1" class="form-control-rc">
                      <div class="spinner-controls">
                          <button type="button" id="print_up" class="up">▲</button>
                          <button type="button" id="print_down" class="down">▼</button>
                      </div>
            </div>
        </div>
      </form>
    `;
        tabsContent.appendChild(newTabPane);
        tabs[newTabId] = {
            qrCode: new QRCodeStyling({
                width: 400,
                height: 400,
                data: "QRPrint",
                image: "",
                dotsOptions: {
                    color: "#6a1a4c",
                    type: "extra-rounded"
                },
                cornersSquareOptions: {
                    color: "#000000",
                    type: "extra-rounded"
                },
                cornersDotOptions: {
                    color: "#000000",
                    type: "dot"
                },
                backgroundOptions: {
                    color: "#ffffff",
                },
                imageOptions: {
                    crossOrigin: "anonymous",
                    margin: 0
                },
                qrOptions: {
                    mode: "Byte",
                    errorCorrectionLevel: "Q"
                }
            }),
            data: {
                data: data,
                title: title,
                subtitle: subtitle,
                customsubtitle: false,
                includeData: false,
                includeTitle: true,
            }
        };

              
        //Mise à jour du panneau affichant le QR Code
        const resultContent = document.getElementById('resultContent');
        const newQRCode = document.createElement('div');
        newQRCode.id = `QRCode-${newTabId}`;
        newQRCode.innerHTML = `<div id="qrContainer-${newTabId}" class="qr-container">
            <div id="qrPreview-${newTabId}" class="qr-preview"></div>
            <div id="qrTitleFrame-${newTabId}" class="qr-title-frame">
              <span id="qrTitleText-${newTabId}">${title}</span>
            </div>
            <div id="qrDataFrame-${newTabId}" class="qr-data-frame">
              <span id="qrDataText-${newTabId}" contenteditable="true">${data}</span>
            </div>
          </div>
          <div class="options-export-group mt-4">
            <div class="d-flex justify-content-between">
              <button type="button" id="export-png-${newTabId}" class="btn btn-success w-48">
                <i class="fas fa-file-export"></i> PNG </button>
              <button type="button" id="export-svg-${newTabId}" class="btn btn-success w-48">
                <i class="fas fa-file-export"></i> SVG </button>
              <button type="button" id="export-clipboard-${newTabId}" class="btn btn-success w-48">
                <i class="fas fa-file-export"></i> COPIER </button>
            </div></div></div>`;
        resultContent.appendChild(newQRCode);

        // Active le nouvel onglet après un court délai pour s'assurer que le DOM est mis à jour
        setTimeout(() => {
            activateTab(newTabId);
        }, 0);
        addFormEventListeners(newTabId);
        renumberTabs();
        multipleOptions.style.display = "block";
        multiple=true;
    }
}


/**
 * Fonction pour renuméroter les onglets après suppression
 */
function renumberTabs() {
    const tabsNav = document.getElementById('qrTabs');
    const tabButtons = tabsNav.querySelectorAll('button.nav-link');
    let number = 1;
    firstTabID="";
    tabButtons.forEach(button => {
        if (button.id === 'add-tab-button') return; // Ignorer le bouton d'ajout (+)

        // Supprime tous les nœuds de texte existants (les labels actuels)
        const textNodes = Array.from(button.childNodes).filter(node => node.nodeType === Node.TEXT_NODE);
        textNodes.forEach(node => button.removeChild(node));

        // Récupère le numéro
        button.insertBefore(document.createTextNode(`${number} `), button.firstChild);
        let tab_id = button.id.replace('-tab', '');
        if (firstTabID==""){firstTabID=tab_id;}
        if (globalPrint) {
            document.getElementById(`print-number-div-${tab_id}`).style.display = "none";
        } else {
            document.getElementById(`print-number-div-${tab_id}`).style.display = "block";
        }
        number++;
    });

    if (number == 2) {
        document.getElementById(`print-number-div-${firstTabID}`).style.display = "none";
        multiple = false;
        multipleOptions.style.display = "none";
    }
    if (!multiple) {
       document.getElementById(`print-number-div-${currentTabId}`).style.display = "none";
    }

}



// Fonction pour supprimer un onglet
function removeTab(tabId, force=false) {
  console.log(tabId);
    const tabsNav = document.getElementById('qrTabs');
    const tabsContent = document.getElementById('qrTabsContent');

    // Ne pas supprimer si c'est le seul onglet
    const totalTabs = tabsNav.querySelectorAll('.nav-item').length - 1; // Exclure le bouton "+"
    if (totalTabs <= 1) {
        if (!force){
        alert("Vous devez conserver au moins un onglet.");}
        return;
    }

    // Supprimer l'onglet de la navigation
    const tabButton = document.getElementById(`${tabId}-tab`);
    const tabTrigger = bootstrap.Tab.getInstance(tabButton);
     if (tabButton && tabButton.parentElement) {
        tabButton.parentElement.remove();
    }

    const contentResult = document.getElementById(`QRCode-${tabId}`);
    if (contentResult) {
        contentResult.remove();
    }

    // Supprimer le contenu de l'onglet
    const tabPane = document.getElementById(tabId);
    if (tabPane) {
        tabPane.remove();
    }

    // Supprimer l'instance QRCodeStyling
    if (tabs[tabId]) {
        delete tabs[tabId];
    }
    setTimeout(() => {
        // Si l'onglet supprimé était actif, activer un autre onglet
        if (currentTabId === tabId) {
            const firstTab = tabsNav.querySelector('.nav-link');
            if (firstTab) {
                const newActiveTabId = firstTab.id.replace('-tab', '');
                setTimeout(() => {
                    activateTab(newActiveTabId);
                }, 0);
            }
        }
    }, 0);
    renumberTabs();
}

// Fonction pour activer un onglet
function activateTab(tabId) {
    try {
        document.getElementById(`QRCode-${currentTabId}`).style.display = "none";
    } catch (error) {

    }
    currentTabId = tabId;
    const qrPreview = document.getElementById(`qrPreview-${tabId}`);

    if (qrPreview) {
    qrPreview.innerHTML = ''; // Effacer le conteneur
    document.getElementById(`QRCode-${tabId}`).style.display = "block";

    // Ajouter le QR Code de l'onglet actif
    tabs[tabId].qrCode.append(qrPreview);
    }
    // Charger les données du formulaire pour l'onglet actif
    loadFormData(tabId);
    updateQRCode(tabId);

    const tabTriggerEl = document.getElementById(`${tabId}-tab`);
    const tab = new bootstrap.Tab(tabTriggerEl);
    tab.show();
    generateImage();
}

// Fonction pour afficher les données de QRCode sélectionné
function loadFormData(tabId) {
    const data = tabs[tabId].data;
    document.getElementById(`form-data-${tabId}`).value = data.data;
    document.getElementById(`form-title-text-${tabId}`).value = data.title;
    document.getElementById(`form-title-include-data-${tabId}`).checked = data.includeData;
    document.getElementById(`form-title-include-title-${tabId}`).checked = data.includeTitle;
}


// ------------------------------------
// Fonctions pour gérer les événements des formulaires et des onglets
// ------------------------------------

// Ajoute des écouteurs d'événements pour le formulaire d'un onglet spécifique
function addFormEventListeners(tabId) {
    const form = document.getElementById(`form-${tabId}`);
    if (!form) return;

    // Écoute les changements dans les inputs et selects
    form.querySelectorAll('input, select').forEach(element => {
        element.addEventListener('input', () => updateQRCode(tabId));
        element.addEventListener('change', () => {
            updateQRCode(tabId);
            generateImage();
        });
    });

    // Sélection automatique du texte lors du clic ou du focus pour tous les inputs
    ['form-data', 'form-title-text', 'form-image-size', 'rows', 'columns'].forEach(id => {
        const elem = document.getElementById(`${id}-${tabId}`);
        if (elem) {
            elem.addEventListener('click', function() {
                this.select();
            });
            elem.addEventListener('focus', function() {
                this.select();
            });
        }
    });


    const dataView = form.querySelectorAll(`#form-title-include-data-${tabId}`).forEach(element => {
         element.addEventListener('change', () => {
    if (element.checked) {
        document.getElementById(`qrDataFrame-${tabId}`).style.display = 'block';    
    } else {
        tabs[tabId].data.customsubtitle=false;
        document.getElementById(`qrDataFrame-${tabId}`).style.display = 'none';
    }
        });
    });




    // Gestion du bouton pour effacer l'image
    const clearImageButton = form.querySelector(`#button-clear-image-${tabId}`);
    if (clearImageButton) {
        clearImageButton.addEventListener("click", function() {
            const imageInput = form.querySelector(`#form-image-file-${tabId}`);
            imageInput.value = "";
            form.querySelector(`#form-image-size-group-${tabId}`).style.display = 'none';
            tabs[tabId].qrCode.update({
                image: ""
            });
            clearImageButton.style.display = 'none';
            generateImage();
        });
    }


    // Bouton pour exporter en PNG
    document.getElementById(`export-png-${tabId}`).addEventListener("click", function() {
        showExportFileNameModal('png', tabId);
    });

    // Bouton pour exporter en SVG
    document.getElementById(`export-svg-${tabId}`).addEventListener("click", function() {
        showExportFileNameModal('svg', tabId);
    });

    document.getElementById(`export-clipboard-${tabId}`).addEventListener("click", function() {
        clipboardCopy(tabId);
    });

    const qrContainer = document.getElementById(`qrContainer-${tabId}`);

    // Fonction pour afficher le menu contextuel
    qrContainer.addEventListener("contextmenu", function(e) {
        e.preventDefault();
        // Positionnement du menu
        customContextMenu.style.top = `${e.clientY}px`;
        customContextMenu.style.left = `${e.clientX}px`;
        customContextMenu.style.display = "block";
    });

    document.getElementById(`${tabId}-tab`).addEventListener('click', function(event) {
        if (event.button === 1) {
            removeTab(tabId);
        }
    });


    document.getElementById(`print-number-${tabId}`).addEventListener('change', async function(e) {
      restrictInput(e,1,99,"number_print");
    });

    document.getElementById(`print-number-${tabId}`).addEventListener('keydown', restrictInputToNumbers);

    document.getElementById(`print-number-${tabId}`).addEventListener('paste', handlePaste);


    const qrTitleSpan = document.getElementById(`qrDataText-${tabId}`);
    qrTitleSpan.addEventListener('input', function() {
        tabs[tabId].data.subtitle = this.textContent;
        tabs[tabId].data.customsubtitle = true;
    });

    qrTitleSpan.addEventListener('blur', function() {
        generateImage();
    });
}



// ------------------------------------
// Gestion des flèches haut et bas des input number
// ------------------------------------

document.addEventListener('click', function(e) {
  if (e.target.matches('.spinner-controls .up')) {
    const input = e.target.closest('.input-number-wrapper').querySelector('input[type="number"]');
    if (input) {
      input.stepUp();
      input.dispatchEvent(new Event('change'));
    }
  } else if (e.target.matches('.spinner-controls .down')) {
    const input = e.target.closest('.input-number-wrapper').querySelector('input[type="number"]');
    if (input) {
      input.stepDown();
      input.dispatchEvent(new Event('change'));
    }
  }
});



// ------------------------------------
// Gestion d'un hash dans l'url
// ------------------------------------

const hash = window.location.hash;

function testUrl() {
  return (hash && hash.length > 1);
}

if (testUrl()) {
  const chaine = decodeURIComponent(hash.substring(1));
  const dataInput = document.getElementById('form-data-tab-1');
  if (dataInput) {
    dataInput.value = chaine;
    updateQRCode('tab-1');
    generateImage();
  }
}



// ------------------------------------
// Gestion de la fenêtre modale d'exportation et de la sauvegarde du QRCode
// ------------------------------------

// Écouteur pour fermer la fenêtre modale d'exportation
closeExportModalBtn.addEventListener('click', () => {
    exportFileNameModal.style.display = 'none';
});

// Fermer la modal en cliquant en dehors du contenu
window.addEventListener('click', (event) => {
    if (event.target == exportFileNameModal) {
        exportFileNameModal.style.display = 'none';
    }
});

// Fonction pour afficher la fenêtre modale de saisie du nom de fichier d'exportation
function showExportFileNameModal(exportType, tabId) {
    const titleText = tabs[tabId].data.title; // Récupère le titre depuis les données de l'onglet
    exportFileNameInput.value = titleText; // Préremplit le champ avec le titre
    exportFileNameModal.style.display = 'block';
    confirmExportBtn.setAttribute('data-export-type', exportType);
    confirmExportBtn.setAttribute('data-tab-id', tabId);
}



// ------------------------------------
// Gestion du menu contextuel personnalisé
// ------------------------------------

// Action pour sauvegarder en PNG
contextSavePng.addEventListener("click", function() {
    customContextMenu.style.display = "none";
    showExportFileNameModal('png', currentTabId); // Affiche la modal pour PNG
});

// Action pour sauvegarder en SVG
contextSaveSvg.addEventListener("click", function() {
    customContextMenu.style.display = "none";
    showExportFileNameModal('svg', currentTabId); // Affiche la modal pour SVG
});

// Action pour copier dans le presse-papier
contextCopyClipboard.addEventListener("click", function() {
    customContextMenu.style.display = "none";
    clipboardCopy(currentTabId);
});

// Cache le menu contextuel lorsqu'on clique ailleurs
document.addEventListener("click", function(e) {
    if (e.button !== 2) { // Si ce n'est pas un clic droit
        customContextMenu.style.display = "none";
    }
});



// ------------------------------------
// Écouteurs pour les boutons d'exportation
// ------------------------------------



// Écouteur pour le bouton de confirmation d'exportation
confirmExportBtn.addEventListener('click', function() {
    const exportType = confirmExportBtn.getAttribute('data-export-type');
    const tabId = confirmExportBtn.getAttribute('data-tab-id');
    const fileName = exportFileNameInput.value || 'qr_code'; // Utilise le nom par défaut si aucun nom n'est donné
    const qrContainer = document.getElementById(`qrContainer-${tabId}`);

    if (exportType === 'png') {
        html2canvas(qrContainer, {
            backgroundColor: null
        }).then(canvas => {
            canvas.toBlob(blob => {
                const url = URL.createObjectURL(blob);
                downloadImage(url, `${fileName}.png`);
            }, 'image/png');
        });
    } else if (exportType === 'svg') {
        tabs[tabId].qrCode.download({
            extension: 'svg',
            name: `${fileName}`
        });
    }

    // Ferme la modal après l'export
    exportFileNameModal.style.display = 'none';
});

// Fonction pour télécharger l'image
function downloadImage(dataUrl, filename) {
    const link = document.createElement('a');
    link.href = dataUrl;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

// ------------------------------------
// Gestion de la copie dans le presse-papier
// ------------------------------------

// Écouteur pour le bouton de copie dans le presse-papier


// Fonction pour détecter si le navigateur est Safari
function isSafari() {
    return /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
}

// Fonction pour copier le QR Code dans le presse-papier
async function clipboardCopy(tabId) {
    customContextMenu.style.display = "none";

    // Vérifie si html2canvas est disponible
    if (typeof html2canvas !== 'function') {
        alert('Erreur : La bibliothèque html2canvas n\'est pas chargée correctement.');
        return;
    }

    // Vérifie si Clipboard API est disponible
    const isClipboardAPIAvailable = !!navigator.clipboard && !!window.ClipboardItem;
    const isSafariBrowser = isSafari();

    const qrContainer = document.getElementById(`qrContainer-${tabId}`);

    if (isClipboardAPIAvailable && !isSafariBrowser) {
        try {
            // Capturer le QR Code et les éléments associés
            const canvas = await html2canvas(qrContainer, {
                backgroundColor: null
            });

            // Convertir le canvas en blob
            const blob = await new Promise((resolve, reject) => {
                canvas.toBlob(blob => {
                    if (blob) {
                        resolve(blob);
                    } else {
                        reject(new Error('Erreur lors de la création du blob pour l\'image.'));
                    }
                }, 'image/png');
            });

            if (!window.ClipboardItem) {
                throw new Error('ClipboardItem n\'est pas supporté.');
            }

            // Crée un ClipboardItem avec le blob de l'image
            const item = new ClipboardItem({
                "image/png": blob
            });

            // Copie l'image dans le presse-papier
            await navigator.clipboard.write([item]);
            alert('Le QRCode a été copié dans le presse-papier.');

        } catch (error) {
            alert('Erreur lors de la copie du QR Code dans le presse-papier.');
        }
    }
    // Safari ne supporte pas la partie précédente, utilisation de execCommand à la place
    else if (isSafariBrowser) {
        try {
            // Capture le QR Code et les éléments associés
            const canvas = await html2canvas(qrContainer, {
                backgroundColor: null
            });

            // Convertit le canvas en data URL
            const dataURL = canvas.toDataURL('image/png');

            // Crée un élément image temporaire
            const img = new Image();
            img.src = dataURL;
            setTimeout(function() {
                img.style.position = 'absolute';
                img.style.left = '-9999px';
                document.body.appendChild(img);

                // Sélectionne l'image
                const range = document.createRange();
                range.selectNode(img);
                window.getSelection().removeAllRanges();
                window.getSelection().addRange(range);

                // Copie l'image via execCommand
                const successful = document.execCommand('copy');

                window.getSelection().removeAllRanges();
                document.body.removeChild(img);

                if (successful) {
                    alert('Le QRCode a été copié dans le presse-papier.');
                } else {
                    throw new Error('La copie automatique a échoué.');
                }
            }, 100);
        } catch (error) {
            alert('Erreur lors de la copie du QR Code dans le presse-papier. Vous pouvez télécharger l\'image en cliquant sur "Exporter en PNG".');
        }
    } else {
        alert('Erreur : Votre navigateur ne supporte pas la copie d\'images dans le presse-papier. Vous pouvez télécharger l\'image en cliquant sur "Exporter en PNG".');
    }
}


// ------------------------------------
// Gestion des Événements des Onglets
// ------------------------------------

// Initialisation des écouteurs pour le premier onglet
addFormEventListeners('tab-1');

// Écouteur pour le changement d'onglet
document.getElementById('qrTabs').addEventListener('click', function(e) {
    const target = e.target.closest('button.nav-link');
    if (target && target.id !== 'add-tab-button') {
        const tabId = target.id.replace('-tab', '');
        activateTab(tabId);
    }
});



// ------------------------------------
// Fonctions utilitaires
// ------------------------------------

// Fonction pour obtenir la largeur du texte
function getTextWidth(text, font) {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    context.font = font;
    const metrics = context.measureText(text);
    return metrics.width;
}

// Fonction pour ajuster la taille de police du titre
function adjustFontSize(tabId) {
    const text = document.getElementById(`qrTitleText-${tabId}`);
    let fontSize = 100; // Commence avec une taille de police de 100%

    let taille = getTextWidth(text.innerText, fontSize * 2 / 100 + 'rem Arial');
    // Réinitialise la taille de police
    text.style.fontSize = fontSize + '%';
    // Réduit la taille de police jusqu'à ce que le texte tienne dans la div
    while (taille > 150 && fontSize > 50) {
        fontSize -= 1;
        text.style.fontSize = fontSize + '%';
        taille = getTextWidth(text.innerText, fontSize * 2 / 100 + 'rem Arial');
    }
}

// ------------------------------------
// Fonction de mise à jour du QR Code et du titre
// ------------------------------------

function updateQRCode(tabId) {
    UI.showLoadingOverlay();

    const form = document.getElementById(`form-${tabId}`);
    if (!form) return;

    // Récupération des valeurs du formulaire
    const data = form.querySelector(`#form-data-${tabId}`).value || "QRPrint";
    const subtitle = tabs[tabId].data.customsubtitle?tabs[tabId].data.subtitle:data;
    const width = 300;
    const margin = 15;

    const dotsType = form.querySelector(`#form-dots-type-${tabId}`).value;
    const dotsColor = form.querySelector(`#form-dots-color-${tabId}`).value;

    const cornersSquareType = form.querySelector(`#form-corners-square-type-${tabId}`).value;
    const cornersSquareColor = form.querySelector(`#form-corners-square-color-${tabId}`).value;

    const cornersDotType = form.querySelector(`#form-corners-dot-type-${tabId}`).value;
    const cornersDotColor = form.querySelector(`#form-corners-dot-color-${tabId}`).value;

    let backgroundColor = form.querySelector(`#form-background-color-${tabId}`).value;

    const hideBackgroundDots = true;
    const imageSize = parseFloat(form.querySelector(`#form-image-size-${tabId}`).value) / 8 || 0.4;
    const imageMargin = 4;
    const imageFile = form.querySelector(`#form-image-file-${tabId}`).files[0];

    const errorCorrectionLevel = form.querySelector(`#form-qr-error-correction-level-${tabId}`).value;

    const qrborderColor = form.querySelector(`#form-title-frame-color-${tabId}`).value;
    const qrTitleColor = form.querySelector(`#form-title-text-color-${tabId}`).value;
    const dataView = form.querySelector(`#form-title-include-data-${tabId}`).checked;

    if (dataView) {
        document.getElementById(`qrDataFrame-${tabId}`).style.display = 'block';    
    } else {
        document.getElementById(`qrDataFrame-${tabId}`).style.display = 'none';
    }

    // Mise à jour du texte du titre
    const titleText = form.querySelector(`#form-title-text-${tabId}`).value || "Scannez-moi";
    document.getElementById(`qrTitleText-${tabId}`).innerText = titleText;
    adjustFontSize(tabId);
    document.getElementById(`qrTitleFrame-${tabId}`).style.width = width + 24 + "px";
    document.getElementById(`qrTitleFrame-${tabId}`).style.height = width / 5 + "px";
    document.getElementById(`qrDataText-${tabId}`).innerText = subtitle;

    const titleView = form.querySelector(`#form-title-include-title-${tabId}`).checked;
    if (titleView) {
        document.getElementById(`qrTitleFrame-${tabId}`).style.display = 'block';
        document.getElementById(`qrTitleFrame-${tabId}`).style.backgroundColor = qrborderColor;
        document.getElementById(`qrTitleFrame-${tabId}`).style.border = '5px solid ' + qrborderColor;
        document.getElementById(`qrTitleFrame-${tabId}`).style.color = qrTitleColor;
        document.getElementById(`qrPreview-${tabId}`).style.border = '12px solid ' + qrborderColor;
        form.querySelector(`#title-input-${tabId}`).style.display = 'block';
        form.querySelector(`#form-border-style-${tabId}`).style.display = 'flex';
        form.querySelector(`#form-title-style-${tabId}`).style.display = 'flex';
    } else {
        document.getElementById(`qrTitleFrame-${tabId}`).style.display = 'none';
        document.getElementById(`qrPreview-${tabId}`).style.border = 'none';
        form.querySelector(`#title-input-${tabId}`).style.display = 'none';
        form.querySelector(`#form-border-style-${tabId}`).style.display = 'none';
        form.querySelector(`#form-title-style-${tabId}`).style.display = 'none';
    }

    const transparentBG = form.querySelector(`#export-transparent-bg-${tabId}`).checked;
    if (transparentBG) {
        form.querySelector(`#form-background-color-div-${tabId}`).style.display = 'none';
        backgroundColor = "rgba(255,255,255,0)";
    } else {
        form.querySelector(`#form-background-color-div-${tabId}`).style.display = 'flex';
    }

    // Préparation des options de mise à jour
    const updateOptions = {
        data: data,
        width: width,
        height: width,
        margin: margin,
        dotsOptions: {
            color: dotsColor,
            type: dotsType
        },
        cornersSquareOptions: {
            color: cornersSquareColor,
            type: cornersSquareType
        },
        cornersDotOptions: {
            color: cornersDotColor,
            type: cornersDotType
        },
        backgroundOptions: {
            color: backgroundColor,
            transparent: transparentBG
        },
        qrOptions: {
            mode: "Byte",
            errorCorrectionLevel: errorCorrectionLevel
        },
        imageOptions: {
            hideBackgroundDots: hideBackgroundDots,
            imageSize: imageSize,
            margin: imageMargin
        }
    };

    // Gestion du fichier image
    if (imageFile) {
        const reader = new FileReader();
        reader.onload = function(e) {
            updateOptions.image = e.target.result;
            tabs[tabId].qrCode.update(updateOptions);
        }
        reader.readAsDataURL(imageFile);
        form.querySelector(`#form-image-size-group-${tabId}`).style.display = 'flex';
        form.querySelector(`#button-clear-image-${tabId}`).style.display = 'flex';

    } else {
        updateOptions.image = "";
        form.querySelector(`#form-image-size-group-${tabId}`).style.display = 'none';
        form.querySelector(`#button-clear-image-${tabId}`).style.display = 'none';
        tabs[tabId].qrCode.update(updateOptions);
    }

    tabs[tabId].data = {
        data: data,
        title: titleText,
        subtitle:subtitle,
        customsubtitle: tabs[tabId].data.customsubtitle,
        includeData: dataView,
        includeTitle: titleView,
    };

    UI.hideLoadingOverlay();
}



// ------------------------------------
// Génération d'image et PDF
// ------------------------------------

function loadImage(img) {

    const thumbnailContainer = document.getElementById('thumbnail');
    thumbnailContainer.innerHTML = ''; // Réinitialise le contenu

    // Crée un conteneur pour l'image
    const imgContainer = document.createElement('div');
    imgContainer.style.position = 'relative'; // Position relative pour le conteneur
    imgContainer.style.display = 'inline-block'; // Permet à l'image d'être dimensionnée par rapport à son contenu
    imgContainer.style.width = '420px'; // Largeur fixe du conteneur
    imgContainer.style.height = 'auto'; // Hauteur automatique pour maintenir le ratio d'aspect
    imgContainer.style.overflow = 'hidden'; // Pour éviter les débordements

    // Ajuster l'image pour qu'elle s'adapte au conteneur
    img.style.width = '100%'; // L'image prend 100% de la largeur du conteneur
    img.style.height = 'auto'; // Maintient le ratio d'aspect

    // Ajoute l'image au conteneur
    imgContainer.appendChild(img);

    // Ajoute le bouton pour générer le PDF
    addButtons(imgContainer);

    // Ajoute le conteneur à la div thumbnail
    thumbnailContainer.appendChild(imgContainer);

}


// Fonction pour générer l'image
async function generateImage() {
    const rows = parseInt(document.getElementById('rows').value);
    const columns = parseInt(document.getElementById('columns').value);

    const margin = 5;
    const dottedlinesValue=dottedlines.checked;

    // Crée un canvas pour dessiner l'image finale
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = 420; // Largeur totale
    canvas.height = 594; // Hauteur totale

    let cellWidth = (canvas.width - (margin * 2)) / columns;
    let cellHeight = (canvas.height - (margin * 2)) / rows;

    // Remplit le canvas avec une couleur de fond si nécessaire
    ctx.fillStyle = "#fff"; // Couleur de fond (blanc)
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // S'il y a plusieurs onglet, une image d'exemple est affichée en fonction des options choisies
    if (multiple) {
        if (!globalPrint) {
            if (!linebreak) {
                const img1 = new Image();
                img1.src = 'img/exemple-grille-1.png';

                img1.onload = function() {
                    loadImage(img1);

                }
            } else {
                const img2 = new Image();
                img2.src = 'img/exemple-grille-2.png';

                img2.onload = function() {

                    loadImage(img2);

                }

            }

        } else {

            if (placementType == "alternated") {

                const img3 = new Image();
                img3.src = 'img/exemple-grille-3.png';

                img3.onload = function() {

                    loadImage(img3);

                }

            } else {

                if (!linebreak) {
                    const img4 = new Image();
                    img4.src = 'img/exemple-grille-4.png';

                    img4.onload = function() {
                        loadImage(img4);

                    }
                } else {
                    const img5 = new Image();
                    img5.src = 'img/exemple-grille-5.png';

                    img5.onload = function() {

                        loadImage(img5);

                    }

                }

            }

        }



    } else {

        const qrCodeContainer = document.getElementById(`qrContainer-${currentTabId}`);
        const width = qrCodeContainer.offsetWidth;
        const height = qrCodeContainer.offsetHeight;


        await new Promise(resolve => setTimeout(resolve, 100));
        // Capture le contenu de la div une seule fois
        const qrcanvas = await html2canvas(qrCodeContainer);
        const imgData = qrcanvas.toDataURL('image/png');
        // Créer un nouvel objet Image pour dessiner sur le canvas
        const img = new Image();
        img.src = imgData;

        // Attend que l'image soit chargée avant de dessiner
        img.onload = () => {
            // Remplit le canvas pour chaque cellule
            for (let i = 0; i < rows; i++) {
                var y = 0;
                if (cellWidth / width > cellHeight / height) {
                    y = i * cellHeight + margin;
                } else {
                    y = i * height / width * cellWidth + margin;
                    cellHeight = height / width * cellWidth;
                }

                if (dottedlinesValue) drawDottedLine2(ctx, 0, y, canvas.width, y);

                for (let j = 0; j < columns; j++) {
                    const x = j * cellWidth + margin;

                    // Dessine l'image sur le canvas final
                    if (cellWidth / width < cellHeight / height) {
                        ctx.drawImage(img, x + 2.5, y + (cellHeight - height / width * (cellWidth - 5)) / 2, cellWidth - 5, height / width * (cellWidth - 5));
                    } else {
                        ctx.drawImage(img, x + (cellWidth - width / height * (cellHeight - 5)) / 2, y + 2.5, width / height * (cellHeight - 5), cellHeight - 5);
                    }

                    if (dottedlinesValue) drawDottedLine2(ctx, x, 0, x, canvas.height);
                }
            }

            // Affiche l'image finale dans une div ou pour le téléchargement
            const finalImg = new Image();
            finalImg.src = canvas.toDataURL('image/png');

            const thumbnailContainer = document.getElementById('thumbnail');
            thumbnailContainer.innerHTML = ''; // Réinitialise le contenu

            // Crée un conteneur pour l'image
            const imgContainer = document.createElement('div');
            imgContainer.style.position = 'relative'; // Position relative pour le conteneur
            imgContainer.style.display = 'inline-block'; // Permet à l'image d'être dimensionnée par rapport à son contenu

            // Ajoute l'image au conteneur
            imgContainer.appendChild(finalImg);

            // Ajoute les boutons
            addButtons(imgContainer);

            // Ajoute le conteneur à la div thumbnail
            thumbnailContainer.appendChild(imgContainer);
        };
    }

    UI.hideLoadingOverlay();
}

// Fonction pour ajouter des boutons sur le thumbnail
function addButtons(container) {
    // Crée le bouton pour imprimer en PDF
    const button = document.createElement('button');
    button.innerText = 'PDF';
    button.style.position = 'absolute'; // Position absolue pour le bouton
    button.classList.add('thumbnail-button');
    button.style.top = '530px';
    button.style.left = '110px';
    button.addEventListener('pointerdown', printPDF);
    // Ajoute le bouton au conteneur
    container.appendChild(button);
}


// Fonction permettant la copie d'un style d'une div
function copyComputedStyle(src, dest) {
    const computedStyle = window.getComputedStyle(src);
    for (let key of computedStyle) {
        dest.style[key] = computedStyle.getPropertyValue(key);
    }
}

// Fonction permettant la copie de tous les styles d'une div
function copyAllComputedStyles(src, dest) {
    copyComputedStyle(src, dest);

    const srcChildren = src.children;
    const destChildren = dest.children;

    for (let i = 0; i < srcChildren.length; i++) {
        copyAllComputedStyles(srcChildren[i], destChildren[i]);
    }
}

// Fonction permettant de cloner une div qui n'est pas affichée pour la copier dans le canvas
async function getHiddenElementDimensions(element) {
    if (!element) {
        return {
            width: 0,
            height: 0
        };
    }

    const clone = element.cloneNode(true);
    copyAllComputedStyles(element, clone);

    // Sélectionne tous les canvas dans l'original et le clone
    const originalCanvases = element.querySelectorAll('canvas');
    const clonedCanvases = clone.querySelectorAll('canvas');

    // Copie le contenu de chaque canvas original vers le clone
    originalCanvases.forEach((originalCanvas, index) => {
        const clonedCanvas = clonedCanvases[index];
        if (clonedCanvas) {
            const originalContext = originalCanvas.getContext('2d');
            const clonedContext = clonedCanvas.getContext('2d');
            if (originalContext && clonedContext) {
                clonedContext.drawImage(originalCanvas, 0, 0);
            }
        }
    });
    clone.style.display = 'block';
    clone.style.top = '-9999px';
    document.body.appendChild(clone);
    const width = clone.offsetWidth;
    const height = clone.offsetHeight;
    const canvas = await html2canvas(clone);
    document.body.removeChild(clone);
    return {
        width,
        height,
        canvas
    };
}

// Fonction pour imprimer en PDF
async function printPDF() {
    UI.showLoadingOverlay();
    await new Promise(resolve => requestAnimationFrame(resolve));
    const rows = parseInt(document.getElementById('rows').value);
    const columns = parseInt(document.getElementById('columns').value);
    const pdf = new jsPDF({
        orientation: 'portrait',
        unit: 'mm', // unités en millimètres
        format: 'a4', // format A4
        putOnlyUsedFonts: true,
        floatPrecision: 16
    });

    const dottedlinesValue=dottedlines.checked;

    const margin = 0;
    let cellWidth = (pdf.internal.pageSize.width - (margin * 2)) / columns; // Largeur de la cellule
    let cellHeight = (pdf.internal.pageSize.height - (margin * 2)) / rows; // Hauteur de chaque cellule


    if (multiple) {

        const tabsNav = document.getElementById('qrTabs');
        const tabButtons = tabsNav.querySelectorAll('button.nav-link');
        let printNumbers = [];
        let width = [];
        let height = [];
        let number = 1;
        let imgData = [];

        for (const button of tabButtons) {
            if (button.id === 'add-tab-button') continue; // Ignorer le bouton d'ajout (+)
            let tab_id = button.id.replace('-tab', '');
            printNumbers[number] = document.getElementById(`print-number-${tab_id}`).value;
            const qrCodeContainer = document.getElementById(`qrContainer-${tab_id}`);
            const dimensions = await getHiddenElementDimensions(qrCodeContainer);
            width[number] = dimensions.width;
            height[number] = dimensions.height;
            const canvas = dimensions.canvas;
            imgData[number] = canvas.toDataURL('image/png');
            number++;
        }
        const total = number - 1;


        if (!globalPrint) {
            number = 0;
            temp_number = 1;
            let total_print = 0;
            printNumbers.forEach(nombre => {
                total_print += nombre;
            });
            nb_page = Math.floor((total_print-1) / (rows * columns)) + 1;
            outerLoop:
                for (let n = 0; n < nb_page; n++) {
                    for (let i = 0; i < rows; i++) {
                        var y = 0;
                        if (cellWidth / width[number % total + 1] > cellHeight / height[number % total + 1]) {
                            y = i * cellHeight + margin;
                        } else {
                            y = i * height[number % total + 1] / width[number % total + 1] * cellWidth + margin;
                            cellHeight = height[number % total + 1] / width[number % total + 1] * cellWidth;
                        }
                        if (dottedlinesValue) drawFullWidthLine(pdf, y);
                        outerLoopInt:
                            for (let j = 0; j < columns; j++) {

                                const x = j * cellWidth + margin;

                                if (cellWidth / width[number % total + 1] < cellHeight / height[number % total + 1]) {
                                    pdf.addImage(imgData[number % total + 1], 'PNG', x + 2.5, y + (cellHeight - height[number % total + 1] / width[number % total + 1] * (cellWidth - 5)) / 2, cellWidth - 5, height[number % total + 1] / width[number % total + 1] * (cellWidth - 5)); // Ajuster les coordonnées et la taille
                                } else {
                                    pdf.addImage(imgData[number % total + 1], 'PNG', x + (cellWidth - width[number % total + 1] / height[number % total + 1] * (cellHeight - 5)) / 2, y + 2.5, width[number % total + 1] / height[number % total + 1] * (cellHeight - 5), cellHeight - 5); // Ajuster les coordonnées et la taille
                                }

                                if (dottedlinesValue) drawFullHeightLine(pdf, x);
                                temp_number++;
                                if (temp_number > printNumbers[number % total + 1]) {
                                    number++;
                                    temp_number = 1;
                                    if (linebreak) break outerLoopInt;
                                }
                                if (number + 1 > total) break outerLoop;

                            }
                        if (number + 1 > total) break outerLoop;
                    }
                    pdf.addPage();
                }


            if (dottedlinesValue) drawFullWidthLine(pdf, rows * cellHeight + margin);
            if (dottedlinesValue) drawFullHeightLine(pdf, columns * cellWidth + margin);
        } else {
            if (placementType == "alternated") {

                number = 0;
                let total_print = globalNumber * total;
                nb_page = Math.floor((total_print-1) / (rows * columns)) + 1;
                outerLoop2:
                    for (let n = 0; n < nb_page; n++) {
                        for (let i = 0; i < rows; i++) {
                            var y = 0;
                            if (cellWidth / width[number % total + 1] > cellHeight / height[number % total + 1]) {
                                y = i * cellHeight + margin;
                            } else {
                                y = i * height[number % total + 1] / width[number % total + 1] * cellWidth + margin;
                                cellHeight = height[number % total + 1] / width[number % total + 1] * cellWidth;
                            }
                            if (dottedlinesValue) drawFullWidthLine(pdf, y);
                            for (let j = 0; j < columns; j++) {

                                const x = j * cellWidth + margin;

                                if (cellWidth / width[number % total + 1] < cellHeight / height[number % total + 1]) {
                                    pdf.addImage(imgData[number % total + 1], 'PNG', x + 2.5, y + (cellHeight - height[number % total + 1] / width[number % total + 1] * (cellWidth - 5)) / 2, cellWidth - 5, height[number % total + 1] / width[number % total + 1] * (cellWidth - 5)); // Ajuster les coordonnées et la taille
                                } else {
                                    pdf.addImage(imgData[number % total + 1], 'PNG', x + (cellWidth - width[number % total + 1] / height[number % total + 1] * (cellHeight - 5)) / 2, y + 2.5, width[number % total + 1] / height[number % total + 1] * (cellHeight - 5), cellHeight - 5); // Ajuster les coordonnées et la taille
                                }

                                if (dottedlinesValue) drawFullHeightLine(pdf, x);
                                number++;

                                if (number + 1 > total_print) break outerLoop2;

                            }
                            if (number + 1 > total_print) break outerLoop2;
                        }
                        pdf.addPage();
                    }


                if (dottedlinesValue) drawFullWidthLine(pdf, rows * cellHeight + margin);
                if (dottedlinesValue) drawFullHeightLine(pdf, columns * cellWidth + margin);




            } else {
                number = 0;
                temp_number = 1;
                let total_print = globalNumber * total;
                nb_page = Math.floor((total_print-1) / (rows * columns)) + 1;
                outerLoop3:
                    for (let n = 0; n < nb_page; n++) {
                        for (let i = 0; i < rows; i++) {
                            var y = 0;
                            if (cellWidth / width[number % total + 1] > cellHeight / height[number % total + 1]) {
                                y = i * cellHeight + margin;
                            } else {
                                y = i * height[number % total + 1] / width[number % total + 1] * cellWidth + margin;
                                cellHeight = height[number % total + 1] / width[number % total + 1] * cellWidth;
                            }
                            if (dottedlinesValue) drawFullWidthLine(pdf, y);
                            outerLoopInt3:
                                for (let j = 0; j < columns; j++) {

                                    const x = j * cellWidth + margin;

                                    if (cellWidth / width[number % total + 1] < cellHeight / height[number % total + 1]) {
                                        pdf.addImage(imgData[number % total + 1], 'PNG', x + 2.5, y + (cellHeight - height[number % total + 1] / width[number % total + 1] * (cellWidth - 5)) / 2, cellWidth - 5, height[number % total + 1] / width[number % total + 1] * (cellWidth - 5)); // Ajuster les coordonnées et la taille
                                    } else {
                                        pdf.addImage(imgData[number % total + 1], 'PNG', x + (cellWidth - width[number % total + 1] / height[number % total + 1] * (cellHeight - 5)) / 2, y + 2.5, width[number % total + 1] / height[number % total + 1] * (cellHeight - 5), cellHeight - 5); // Ajuster les coordonnées et la taille
                                    }

                                    if (dottedlinesValue) drawFullHeightLine(pdf, x);
                                    temp_number++;
                                    if (temp_number > globalNumber) {
                                        number++;
                                        temp_number = 1;
                                        if (linebreak) break outerLoopInt3;
                                    }
                                    if (number + 1 > total) break outerLoop3;

                                }
                            if (number + 1 > total) break outerLoop3;
                        }
                        pdf.addPage();
                    }


                if (dottedlinesValue) drawFullWidthLine(pdf, rows * cellHeight + margin);
                if (dottedlinesValue) drawFullHeightLine(pdf, columns * cellWidth + margin);
            }
        }

    } else {

        const qrCodeContainer = document.getElementById(`qrContainer-${currentTabId}`);
        const width = qrCodeContainer.offsetWidth;
        const height = qrCodeContainer.offsetHeight;

        // Capture le contenu de la div en tant qu'image
        const canvas = await html2canvas(qrCodeContainer);
        const imgData = canvas.toDataURL('image/png');
        for (let i = 0; i < rows; i++) {
            var y = 0;
            if (cellWidth / width > cellHeight / height) {
                y = i * cellHeight + margin;
            } else {
                y = i * height / width * cellWidth + margin;
                cellHeight = height / width * cellWidth;
            }
            if (dottedlinesValue) drawFullWidthLine(pdf, y);
            for (let j = 0; j < columns; j++) {
                const x = j * cellWidth + margin;

                if (cellWidth / width < cellHeight / height) {
                    pdf.addImage(imgData, 'PNG', x + 2.5, y + (cellHeight - height / width * (cellWidth - 5)) / 2, cellWidth - 5, height / width * (cellWidth - 5)); // Ajuster les coordonnées et la taille
                } else {
                    pdf.addImage(imgData, 'PNG', x + (cellWidth - width / height * (cellHeight - 5)) / 2, y + 2.5, width / height * (cellHeight - 5), cellHeight - 5); // Ajuster les coordonnées et la taille
                }

                if (dottedlinesValue) drawFullHeightLine(pdf, x);
            }
        }
        if (dottedlinesValue) drawFullWidthLine(pdf, rows * cellHeight + margin);
        if (dottedlinesValue) drawFullHeightLine(pdf, columns * cellWidth + margin);
    }

    const blob = pdf.output('blob'); // Récupère le PDF sous forme de blob
    const blobUrl = URL.createObjectURL(blob); // Crée une URL pour le blob

    // Ouvre le blob dans un nouvel onglet
    const printWindow = window.open(blobUrl, '_blank'); // Ouvre l'URL du blob

    if (printWindow) {
        printWindow.focus(); // Met le focus sur le nouvel onglet
    } else {
        alert('Veuillez autoriser les pop-ups pour imprimer le PDF.'); // Alerte si le pop-up est bloqué
    }

    UI.hideLoadingOverlay();
}

// Fonction pour dessiner une ligne pointillée sur le canvas
function drawDottedLine2(ctx, x1, y1, x2, y2) {
    const dashLength = 2; // Longueur du trait
    const gapLength = 2; // Longueur de l'espace
    const totalLength = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
    const dashes = Math.floor(totalLength / (dashLength + gapLength));
    ctx.beginPath();
    for (let i = 0; i < dashes; i++) {
        const x = x1 + (i * (dashLength + gapLength) * (x2 - x1) / totalLength);
        const y = y1 + (i * (dashLength + gapLength) * (y2 - y1) / totalLength);
        ctx.lineTo(x, y);
        ctx.moveTo(x + dashLength * (x2 - x1) / totalLength, y + dashLength * (y2 - y1) / totalLength);
    }
    ctx.strokeStyle = 'black'; // Couleur des lignes
    ctx.lineWidth = 0.5; // Épaisseur des lignes
    ctx.stroke(); // Dessine les lignes
}

// Fonctions pour dessiner des lignes pointillées complètes sur le PDF
function drawFullWidthLine(pdf, y) {
    const startX = 0; // Début de la ligne à la bordure gauche
    const endX = pdf.internal.pageSize.width; // Fin de la ligne à la bordure droite
    drawDottedLine(pdf, startX, y, endX, y);
}

function drawFullHeightLine(pdf, x) {
    const startY = 0; // Début de la ligne à la bordure supérieure
    const endY = pdf.internal.pageSize.height; // Fin de la ligne à la bordure inférieure
    drawDottedLine(pdf, x, startY, x, endY);
}

// Fonction pour dessiner une ligne pointillée sur le PDF
function drawDottedLine(pdf, x1, y1, x2, y2) {
    const dashLength = 2; // Longueur du trait
    const gapLength = 2; // Longueur de l'espace
    const totalLength = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
    const dashes = Math.floor(totalLength / (dashLength + gapLength));

    for (let i = 0; i < dashes; i++) {
        const x = x1 + (i * (dashLength + gapLength) * (x2 - x1) / totalLength);
        const y = y1 + (i * (dashLength + gapLength) * (y2 - y1) / totalLength);
        pdf.line(x, y, x + dashLength * (x2 - x1) / totalLength, y + dashLength * (y2 - y1) / totalLength);
    }
}

// ------------------------------------
// Gestion des options de génération de la grille
// ------------------------------------

useGlobalQuantityCheckbox.addEventListener('change', function() {
    if (this.checked) {
        globalQuantityContainer.style.display = 'flex';
        placementTypeContainer.style.display = 'flex';
        updateLineBreakVisibility();
    } else {
        globalQuantityContainer.style.display = 'none';
        placementTypeContainer.style.display = 'none';
        lineBreakContainer.style.display = 'flex';
    }
    globalPrint = useGlobalQuantityCheckbox.checked;
    renumberTabs();
    generateImage();

});

globalQuantityInput.addEventListener('change', function() {
    globalNumber = globalQuantityInput.value;
    generateImage();
});


placementTypeSelect.addEventListener('change', function() {
    updateLineBreakVisibility();
    placementType = placementTypeSelect.value;
    generateImage();
});


lineBreakCheckbox.addEventListener('change', function() {
    updateLineBreakVisibility();
    generateImage();
});

dottedlines.addEventListener('change', function() {
    generateImage();
});


// ------------------------------------
// Gestion du mode multiple
// ------------------------------------

//Affiche ou non les éléments pour gérer les options des QR Codes multiples
MultipleQRCodeMode.addEventListener('change', function() {
  if (MultipleQRCodeMode.checked) {
    multiple=true;
  document.getElementById('multipleQRCode').style.display='block';
  multipleOptions.style.display = "block";
  renumberTabs();
  generateImage();
}
else
{
  multiple=false;
  document.getElementById('multipleQRCode').style.display='none';
  multipleOptions.style.display = "none";
  renumberTabs();
  generateImage();

}
});


// Fonction pour mettre à jour la visibilité de la checkbox de retour à la Ligne
function updateLineBreakVisibility() {
    if ((placementTypeSelect.value === 'grouped' && useGlobalQuantityCheckbox.checked) || (!useGlobalQuantityCheckbox.checked)) {
        lineBreakContainer.style.display = 'flex';
    } else {
        lineBreakContainer.style.display = 'none';
        lineBreakCheckbox.checked = false;
    }
    linebreak = lineBreakCheckbox.checked;
}


// ------------------------------------
// Gestion du pied de page avec les menus accordéons
// ------------------------------------

const accordions = document.querySelectorAll(".accordionFooter");
accordions.forEach(accordion => {
    accordion.addEventListener("click", function() {
        this.classList.toggle("active"); // Alterne la classe 'active' pour le style
        const panel = this.nextElementSibling; // Sélectionne le panneau associé
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null; // Réduit le panneau s'il est ouvert
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px"; // Ouvre le panneau
        }
    });
});



// ------------------------------------
// Gestion de l'import tableur et CSV
// ------------------------------------

// Lorsque l'utilisateur sélectionne un fichier CSV
csvFileInput.addEventListener('change', function(event) {
  const file = event.target.files[0];
  if (!file) return;

  // Vérifie le type de fichier
  if (file.type !== 'text/csv' && !file.name.endsWith('.csv')) {
    alert('Veuillez sélectionner un fichier CSV.');
    return;
  }

  const reader = new FileReader();
  reader.onload = function(e) {
    const csvText = e.target.result;
    csvFileInput.value = '';

    // Utilise PapaParse pour parser le CSV
    if (typeof Papa !== 'undefined') {
      const parsed = Papa.parse(csvText, {
        header: false,
        skipEmptyLines: true
      });

      if (parsed.errors.length > 0) {
        console.error(parsed.errors);
        alert('Erreur lors du parsing du fichier CSV.');
        return;
      }

      // Reconstruit le contenu avec des tabulations comme séparateurs
      const tabSeparatedText = parsed.data.map(row => row.join('\t')).join('\n');

      // Remplit la zone de texte avec le contenu tabulé
      csvDataTextarea.value = tabSeparatedText;
    } else {
      // Si PapaParse n'est pas disponible, fait un remplacement simple
      const tabSeparatedText = csvText.replace(/,/g, '\t');
      csvDataTextarea.value = tabSeparatedText;
    }
  };
  reader.onerror = function() {
    alert('Erreur lors de la lecture du fichier CSV.');
  };
  reader.readAsText(file);
});


// Ajout de l'écouteur d'événement pour la touche Tab
csvDataTextarea.addEventListener('keydown', function(e) {
    if (e.key === 'Tab') {
        e.preventDefault(); // Empêche le comportement par défaut (navigation)
        
        const start = this.selectionStart;
        const end = this.selectionEnd;

        // Insère une tabulation à la position du curseur
        this.value = this.value.substring(0, start) + '\t' + this.value.substring(end);

        // Déplace le curseur après la tabulation
        this.selectionStart = this.selectionEnd = start + 1;
    }
});


// Lorsque l'utilisateur clique sur le bouton Valider
validateImportBtn.addEventListener('click', function() {
  const csvText = csvDataTextarea.value.trim();
  if (!csvText) {
    alert('Veuillez saisir ou importer des données CSV.');
    return;
  }

  // Ferme la modale
  const modal = bootstrap.Modal.getInstance(importCsvModal);
  modal.hide();

  // Traite les données CSV
  handleCsvImport(csvText);
});



function handleCsvImport(csvText) {
  const existingTabs = document.querySelectorAll('#qrTabs li').length - 1; // Exclure le bouton "+"
  if (existingTabs >= 1) {
    const userConfirmed = confirm(`L'importation de nouvelles données CSV va supprimer les onglets existants. Voulez-vous continuer ?`);
    if (!userConfirmed) {
      return; // Annule l'importation
    }
    // Supprime tous les onglets existants
    const tabsNav = document.getElementById('qrTabs');
    const tabsContent = document.getElementById('qrTabsContent');
    firstTabID="";
    tabsNav.querySelectorAll('li.nav-item').forEach(li => {
      const button = li.querySelector('button.nav-link');
      if (button) {
        const tabId = button.id.replace('-tab', '');
        if (firstTabID==""){firstTabID=tabId;}
        removeTab(tabId, true);
      }
    });
     tabCount = 1;
     currentTabId = firstTabID;
     
  }

  // Procède à l'importation des nouvelles données
  processCsvData(csvText);
  removeTab(firstTabID,true);
}

function processCsvData(csvText) {
  // Divise le texte en lignes
  const lines = csvText.split('\n').filter(line => line.trim() !== '');
  const maxTabs = 20;
  const rows = lines.slice(0, maxTabs);

  let addedTabs = 0;
  let invalidRows = 0;

  rows.forEach((line, index) => {
    // Divise chaque ligne en colonnes en utilisant les tabulations comme séparateurs
    const columns = line.split('\t');
    const qrData = columns[0] ? columns[0].trim() : '';
    const title = columns[1] ? columns[1].trim() : 'Scannez-moi';
    let number = columns[2] && !isNaN(parseInt(columns[2], 10)) ? parseInt(columns[2], 10) : 1;

    if (!qrData) {
      console.warn(`Ligne ${index + 1} : Données QR manquantes.`);
      invalidRows++;
      return;
    }

    // Valide le nombre d'impressions
    if (number < 1 || number > 100) {
      console.warn(`Ligne ${index + 1} : Nombre d'impressions invalide. Utilisation de la valeur par défaut (1).`);
      number = 1;
    }

    // Ajoute l'onglet avec les données
    addTab(qrData, title, number, true);
    addedTabs++;
  });

  let message = `${addedTabs} onglet(s) ont été ajouté(s) à partir des données CSV.`;
  if (invalidRows > 0) {
    message += `\n${invalidRows} ligne(s) ont été ignorée(s) en raison de données manquantes.`;
  }
  alert(message);

}


// ------------------------------------
// Appels initiaux
// ------------------------------------

// Génération initiale du QRCode et de l'image
updateQRCode('tab-1');
generateImage();